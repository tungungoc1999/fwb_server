package com.example.fwbchat.Server;

import com.example.fwbchat.Entity.GroupChat;
import com.example.fwbchat.Entity.MemberGroup;
import com.example.fwbchat.Entity.MessageChat;
import com.example.fwbchat.Entity.UserInfo;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Data {

    //group
    @GET("group.php")
    Call<List<GroupChat>> getListChat();

    @FormUrlEncoded
    @POST("groupById.php")
    Call<List<GroupChat>> getListGroupChat(@Field("idG") String idG);

    @FormUrlEncoded
    @POST("groupByIdThanhVien.php")
    Call<List<GroupChat>> getListGroupChatByIdThanhVien(@Field("idG") String idG);

    @FormUrlEncoded
    @POST("addGroup.php")
    Call<String> createGroup(@Field("TenNhom") String TenNhom, @Field("NgayTao") String NgayTao, @Field("Type") String Type, @Field("IdNguoiTao") String IdNguoiTao, @Field("TenNguoiTao") String TenNguoiTao, @Field("MoTa") String MoTa);

    //member
    @FormUrlEncoded
    @POST("MemberINGroup.php")
    Call<List<UserInfo>> getListMemberInGroup(@Field("idnhom") String IdNhom);

    @FormUrlEncoded
    @POST("MemberOUTGroup.php")
    Call<List<UserInfo>> getListMemberOutGroup(@Field("idnhom") String IdNhom);

    @FormUrlEncoded
    @POST("MemberDeleteGroup.php")
    Call<String> deleteMemberGroup(@Field("idnhom") String idnhom,@Field("idthanhvien") String idthanhvien);

    @FormUrlEncoded
    @POST("addMemberGroup.php")
    Call<String> addMember(@Field("IdNhom") String IdNhom,@Field("IdThanhVien") String IdThanhVien,@Field("ChucVu") String ChucVu,@Field("NgayVao") String NgayVao);


    //message
    @FormUrlEncoded
    @POST("message.php")
    Call<List<MessageChat>> getListMessage(@Field("Id") String idNguoiGui, @Field("IdConver") String idNguoiNhan, @Field("Type") String Kieu);

    @FormUrlEncoded
    @POST("messageGroup.php")
    Call<List<MessageChat>> getListMessageGroup(@Field("IdConver") String IdConver);

    @FormUrlEncoded
    @POST("addMessage.php")
    Call<Void> sendMessage(@Field("Message") String mess,@Field("IdNguoiGui") String IdNguoiGui,@Field("TenNguoiGui") String TenNguoiGui,
                            @Field("KieuTinNhan") String KieuTinNhan,@Field("IdNguoiNhan") String IdNguoiNhan,@Field("TenNguoiNhan") String TenNguoiNhan,
                            @Field("ThoiGianGui") String ThoiGianGui,@Field("TrangThai") String TrangThai,@Field("CountChuaDoc") String CountChuaDoc);

    //user
    @FormUrlEncoded
    @POST("checkLogin.php")
    Call<List<UserInfo>> checkLogin(@Field("taikhoan") String taikhoan, @Field("matkhau") String matkhau);

    @FormUrlEncoded
    @POST("checkResgister.php")
    Call<String> checkRegister(@Field("taikhoan") String taikhoan);

    @FormUrlEncoded
    @POST("addUser.php")
    Call<String> Register(@Field("TaiKhoan") String taikhoan,@Field("MatKhau") String MatKhau,@Field("HoTen") String HoTen);

    @GET("ListUserAll.php")
    Call<List<UserInfo>> getAllUser();

    @FormUrlEncoded
    @POST("ListUser.php")
    Call<List<UserInfo>> getAllUser(@Field("idUser") String idUser);

    @FormUrlEncoded
    @POST("getUserInfo.php")
    Call<List<UserInfo>> getUserInfo(@Field("idUser") String idUser);

    @FormUrlEncoded
    @POST("updateProfile.php")
    Call<String> updateProfile(@Field("IdUser") String IdUser, @Field("HoTen") String HoTen, @Field("MoiQuanHe") String MoiQuanHe, @Field("NoiSong") String NoiSong, @Field("SoDT") String SoDT, @Field("NoiLamViec") String NoiLamViec, @Field("GioiThieu") String GioiThieu);

//    @FormUrlEncoded
//    @POST("danhsachbaihat.php")
//    Call<List<BaiHat>> getDanhSachBaiHatTheoQuangCao(@Field("idquangcao") String idQuangCao);
//
//    @FormUrlEncoded
//    @POST("danhsachbaihat.php")
//    Call<List<BaiHat>> getDanhSachBaiHatTheoPlayList(@Field("idplaylist") String idPlayList);
//
//
//    @GET("danhsachplaylist.php")
//    Call<List<PlayList>> getDanhSachPlaylist();
//
//    @FormUrlEncoded
//    @POST("danhsachbaihat.php")
//    Call<List<BaiHat>> getDanhSachBaiHatTheoTheLoai(@Field("idtheloai") String idTheLoai);
//
//    @GET("tatcachude.php")
//    Call<List<ChuDe>> getAllChuDe();
//
//    @FormUrlEncoded
//    @POST("theloaitheochude.php")
//    Call<List<TheLoai>> getTheLoaiTheoChuDe(@Field("idchude") String idChuDe);
//
//    @GET("tatcaalbum.php")
//    Call<List<Album>> getAlbum();
//
//    @FormUrlEncoded
//    @POST("danhsachbaihat.php")
//    Call<List<BaiHat>> getBaiHatTheoAlbum(@Field("idalbum") String idAlbum);
//
//    @FormUrlEncoded
//    @POST("updateluotthich.php")
//    Call<String> updateLuotThich(@Field("luotthich") String luotthich, @Field("idbaihat") String idbaihat);
//
//    @FormUrlEncoded
//    @POST("timkiembaihat.php")
//    Call<List<BaiHat>> getSearchBaiHat(@Field("tukhoa") String tuKhoa);
}
