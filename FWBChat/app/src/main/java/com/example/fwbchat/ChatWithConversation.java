package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.fwbchat.Adapter.MessageAdapter;
import com.example.fwbchat.Entity.MessageChat;
import com.example.fwbchat.Server.APIService;
import com.example.fwbchat.Server.Data;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatWithConversation extends AppCompatActivity {
    private ArrayList<MessageChat> mlist;
    private MessageAdapter mAdapter;
    private ImageView btnSend;
    private TextView textMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_with_conversation);
        TextView tv_name = findViewById(R.id.tv_name);
        ListView lv1= findViewById(R.id.list_mess);
        btnSend = findViewById(R.id.ivSend);
        textMessage = findViewById(R.id.etComposeBox);
        mlist = new ArrayList<MessageChat>();
        lv1.setDivider(null);
        lv1.setDividerHeight(0);
        SharedPreferences preferences = getSharedPreferences("userLogin", Context.MODE_PRIVATE);
        String uid = preferences.getString("uid","");
        String name = preferences.getString("ten","");
        String idConversation = preferences.getString("idConversation","");
        String nameConversation = preferences.getString("nameConversation","");
        tv_name.setText(nameConversation);

        Data data = APIService.getService();
        Call<List<MessageChat>> callback = data.getListMessage(uid,idConversation,"User");
        callback.enqueue(new Callback<List<MessageChat>>() {
            @Override
            public void onResponse(Call<List<MessageChat>> call, Response<List<MessageChat>> response) {
                mlist = (ArrayList<MessageChat>) response.body();
                mAdapter = new MessageAdapter(ChatWithConversation.this, mlist);
                lv1.setAdapter(mAdapter);
                lv1.setSelection(mAdapter.getCount()-1);
            }

            @Override
            public void onFailure(Call<List<MessageChat>> call, Throwable t) {

            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mess = textMessage.getText().toString();
                if(!mess.isEmpty()){

                    Call<Void> call = data.sendMessage(mess,uid,name,"User",idConversation,nameConversation,"2021-02-26 13:47:00.000","Đã xem","1");
                    call.enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            Call<List<MessageChat>> callback = data.getListMessage(uid,idConversation,"User");
                            callback.enqueue(new Callback<List<MessageChat>>() {
                                @Override
                                public void onResponse(Call<List<MessageChat>> call, Response<List<MessageChat>> response) {
                                    mlist = (ArrayList<MessageChat>) response.body();
                                    mAdapter = new MessageAdapter(ChatWithConversation.this, mlist);
                                    lv1.setAdapter(mAdapter);
                                    lv1.setSelection(mAdapter.getCount()-1);
                                }

                                @Override
                                public void onFailure(Call<List<MessageChat>> call, Throwable t) {

                                }
                            });
                        }
                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {

                        }
                    });
                    textMessage.setText("");
                }
            }
        });
    }
    public void backToListConversation(View view) {
        startActivity(new Intent(ChatWithConversation.this,HomeActivity.class));
    }
}