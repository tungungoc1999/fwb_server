package com.example.fwbchat.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MemberGroup {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("IdNhom")
    @Expose
    private String idNhom;
    @SerializedName("IdThanhVien")
    @Expose
    private String idThanhVien;
    @SerializedName("ChucVu")
    @Expose
    private String chucVu;
    @SerializedName("NgayVao")
    @Expose
    private String ngayVao;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdNhom() {
        return idNhom;
    }

    public void setIdNhom(String idNhom) {
        this.idNhom = idNhom;
    }

    public String getIdThanhVien() {
        return idThanhVien;
    }

    public void setIdThanhVien(String idThanhVien) {
        this.idThanhVien = idThanhVien;
    }

    public String getChucVu() {
        return chucVu;
    }

    public void setChucVu(String chucVu) {
        this.chucVu = chucVu;
    }

    public String getNgayVao() {
        return ngayVao;
    }

    public void setNgayVao(String ngayVao) {
        this.ngayVao = ngayVao;
    }

}
