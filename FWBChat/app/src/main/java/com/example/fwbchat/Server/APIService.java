package com.example.fwbchat.Server;

public class APIService {
    private static String base_url = "https://fwbchat.000webhostapp.com/Server/";

    public static Data getService() {
        return APIRetrofitClient.getClient(base_url).create(Data.class);
    }
}
