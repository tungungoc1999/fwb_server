package com.example.fwbchat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.ListFragment;

import com.example.fwbchat.Adapter.UserAdapter;
import com.example.fwbchat.Entity.UserInfo;
import com.example.fwbchat.Server.APIService;
import com.example.fwbchat.Server.Data;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class FragmentConversation extends ListFragment {
    private ArrayList<UserInfo> mlist;
    private UserAdapter mAdapter;
    private View view;
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mlist = new ArrayList<UserInfo>();
        SharedPreferences preferences = this.getActivity().getSharedPreferences("userLogin", Context.MODE_PRIVATE);
        String uid = preferences.getString("uid","");

        Data data = APIService.getService();
        Call<List<UserInfo>> callback = data.getAllUser(uid);
        callback.enqueue(new Callback<List<UserInfo>>() {
            @Override
            public void onResponse(Call<List<UserInfo>> call, Response<List<UserInfo>> response) {
                mlist = (ArrayList<UserInfo>) response.body();
                ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1,mlist);
                setListAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<UserInfo>> call, Throwable t) {

            }
        });
        return inflater.inflate(R.layout.list_item_conversation,container,false);
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        SharedPreferences sharedPref;
        SharedPreferences.Editor editor;
        sharedPref = getActivity().getSharedPreferences("userLogin",MODE_PRIVATE);
        editor = sharedPref.edit();
        UserInfo user = mlist.get(position);
        editor.putString("idConversation",user.getId());
        editor.putString("nameConversation",user.getHoTen());
        editor.commit();
        Intent intent = new Intent(getActivity(),ChatWithConversation.class);
        startActivity(intent);
        super.onListItemClick(l, v, position, id);
    }
}
