package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fwbchat.Entity.GroupChat;
import com.example.fwbchat.Entity.MemberGroup;
import com.example.fwbchat.Entity.UserInfo;
import com.example.fwbchat.Server.APIService;
import com.example.fwbchat.Server.Data;
import com.google.android.material.appbar.MaterialToolbar;

import java.sql.SQLException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailGroup extends AppCompatActivity {
    private TextView tv_group_name,group_description,tv_delete,tv_exit;
    private Context context = DetailGroup.this;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private RelativeLayout rl_add_member,rl_delete_member;
    private MaterialToolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_group);
        tv_group_name = findViewById(R.id.tv_group_name);
        group_description = findViewById(R.id.group_description);
        rl_add_member = findViewById(R.id.rl_add_member);
        rl_delete_member = findViewById(R.id.rl_delete_member);
        tv_delete = findViewById(R.id.tv_delete);
        tv_exit = findViewById(R.id.tv_exit);
        toolbar = findViewById(R.id.groupDetailToolbar);
        Data data = APIService.getService();
        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        String idGroup = sharedPref.getString("idGroup","");
        String uid = sharedPref.getString("uid","");
        String nameGroup = sharedPref.getString("nameGroup","");
//        GroupChat group = new GroupChat();
//        ArrayList<MemberGroup> lstMem = new ArrayList<MemberGroup>();
//        ArrayList<UserInfo> lstUser = new ArrayList<UserInfo>();
//        try {
//             group = groupModel.findById(idGroup);
//            lstMem = memberModel.getListMemberGroup(group.getId());
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        if(group != null){
//            tv_group_name.setText(group.getTenNhom());
//            group_description.setText(group.getMota());
//            if(!group.getIdNguoiTao().equals(uid)){
//                rl_add_member.setVisibility(View.GONE);
//                tv_delete.setVisibility(View.GONE);
//                rl_delete_member.setVisibility(View.GONE);
//            }
//        }
        tv_group_name.setText(nameGroup);

        rl_add_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailGroup.this,AddMember.class));
            }
        });
        rl_delete_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailGroup.this,DeleteMember.class));
            }
        });
        tv_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<String> deleteM = data.deleteMemberGroup(idGroup,uid);
                deleteM.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String res = response.body();
                        if(res.equals("Success")){
                            Toast.makeText(DetailGroup.this,"Rời nhóm thành công" , Toast.LENGTH_LONG).show();
                            startActivity(new Intent(DetailGroup.this,HomeActivity.class));
                        }
                    }
                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });
            }
        });
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DetailGroup.this,ChatWithGroup.class));
            }
        });
    }
}