package com.example.fwbchat.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.fwbchat.Entity.MessageChat;
import com.example.fwbchat.R;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class MessageAdapter extends ArrayAdapter<MessageChat> {
    private Context context = getContext();
    private int resource;
    private ArrayList<MessageChat> ar;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    public MessageAdapter(Context context, ArrayList<MessageChat> countryList) {
        super(context, 0, countryList);
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        sharedPref = this.context.getSharedPreferences("userLogin",MODE_PRIVATE);
        String uid = sharedPref.getString("uid","");
        MessageChat currentItem = getItem(position);
        if(currentItem !=null){
            if(currentItem.getIdNguoiGui().equals(uid)){
                convertView = LayoutInflater.from(getContext()).inflate(
                        R.layout.item_message_text_right, parent, false
                );
            }else{
                convertView = LayoutInflater.from(getContext()).inflate(
                        R.layout.item_message_text_left, parent, false
                );
                if(position > 0){
                    MessageChat currentItemBefore = getItem(position-1);
                    if(!currentItemBefore.getIdNguoiGui().equals(currentItem.getIdNguoiGui())){
                        TextView txtUser = convertView.findViewById(R.id.tv_user);
                        txtUser.setText(currentItem.getTenNguoiGui());
                    }else{
                        TextView txtUser = convertView.findViewById(R.id.tv_user);
                        txtUser.setVisibility(View.GONE);
                    }
                }else{
                    TextView txtUser = convertView.findViewById(R.id.tv_user);
                    txtUser.setText(currentItem.getTenNguoiGui());
                }
            }
            TextView txtUserName = convertView.findViewById(R.id.go_txt_message);
            txtUserName.setText(currentItem.getMessage());
//            TextView txtTime = convertView.findViewById(R.id.txt_time);
//            txtTime.setText("9:00PM");

        }
        return convertView;
    }
}
