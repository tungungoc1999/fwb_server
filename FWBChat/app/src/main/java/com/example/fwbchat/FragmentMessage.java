package com.example.fwbchat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.fwbchat.Adapter.MessageAdapter;
import com.example.fwbchat.Entity.MessageChat;

import java.util.ArrayList;

public class FragmentMessage extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<MessageChat> mlist;
    private MessageAdapter mAdapter;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    public FragmentMessage() {
    }
    public static FragmentUser newInstance(String param1, String param2) {
        FragmentUser fragment = new FragmentUser();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.list_item_message, container, false);
//        TextView tv_name = view.findViewById(R.id.tv_name);
//        ListView lv1= view.findViewById(R.id.list_mess);
//        mlist = new ArrayList<Message>();
//        lv1.setDivider(null);
//        lv1.setDividerHeight(0);
//        SharedPreferences preferences = this.getActivity().getSharedPreferences("userLogin", Context.MODE_PRIVATE);
//        String uid = preferences.getString("uid","");
//        String idConversation = preferences.getString("idConversation","");
//        String nameConversation = preferences.getString("nameConversation","");
//        tv_name.setText(nameConversation);
//        try {
//            if(uid !="") {
//                mlist = DAO.getListMessage(uid,idConversation,"User");
//            }
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        mAdapter = new MessageAdapter(this.getContext(), mlist);
//        lv1.setAdapter(mAdapter);
        return view;
    }
}
