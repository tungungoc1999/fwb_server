package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.fwbchat.Entity.UserInfo;
import com.example.fwbchat.Server.APIService;
import com.example.fwbchat.Server.Data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Create_Group extends AppCompatActivity {
    private EditText txtName;
    private Button btnCreate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create__group);
        txtName = findViewById(R.id.group_name);
        btnCreate = findViewById(R.id.btn_create_group);
        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtName.getText().toString().trim();
                if(name.isEmpty()){
                    Toast.makeText(Create_Group.this,"Tên nhóm không được để trống" , Toast.LENGTH_LONG).show();
                }else{
                    SharedPreferences preferences = getSharedPreferences("userLogin", Context.MODE_PRIVATE);
                    String uidUser = preferences.getString("uid","");
                    String nameUser = preferences.getString("ten","");

                    Data data = APIService.getService();
                    Call<String> callback = data.createGroup(name,"2021-02-26 13:47:00.000","Private",uidUser,nameUser,"");
                    callback.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            String mess = response.body();
                            if(mess.equals("Success")){
                                Toast.makeText(Create_Group.this,"Tạo nhóm thành công" , Toast.LENGTH_LONG).show();
                                startActivity(new Intent(Create_Group.this,HomeActivity.class));
                            }else {
                                Toast.makeText(Create_Group.this,"Tạo nhóm thất bại" , Toast.LENGTH_LONG).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(Create_Group.this,"Lỗi", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }
}