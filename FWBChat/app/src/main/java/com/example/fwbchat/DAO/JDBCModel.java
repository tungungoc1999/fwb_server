package com.example.fwbchat.DAO;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.util.Log;
import android.widget.SimpleAdapter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;

@SuppressLint("NewApi")
public class JDBCModel {
    static private final String serverName = "192.168.0.111";
    static private final String dbName = "FWB";
    static private final String portNumber = "1433";
    static private final String userID = "sa";
    static private final String password = "1";
    static private final String classs = "net.sourceforge.jtds.jdbc.Driver";
    public JDBCModel(){

    }
    @SuppressLint("NewApi")
    public static Connection Connect() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String ConnURL = null;
        try {

            Class.forName(classs);
            ConnURL = "jdbc:jtds:sqlserver://" + serverName +";databaseName="+ dbName + ";user=" + userID+ ";password=" + password + ";";
            //ConnURL="jdbc:mysql://{hostname}:{port}"
            String sConnURL = "jdbc:jtds:sqlserver://"
                    + serverName + ":" + portNumber + ";"
                    + "databaseName=" + dbName
                    + ";user=" + userID
                    + ";password=" + password + ";";
            conn = DriverManager.getConnection(sConnURL);
            ///Connection conn2 = DriverManager.getConnection(url, username, password);

        } catch (SQLException se) {
            Log.e("ERROR1","Không thể tải lớp Driver! "+ se.getMessage());
        } catch (ClassNotFoundException e) {
            Log.e("ERROR2","Xuất hiện vấn đề truy cập trong khi tải! "+ e.getMessage());
        } catch (Exception e) {
            Log.e("ERROR3", "Không thể khởi tạo Driver! "+e.getMessage());
        }
        return conn;
    }
}
