package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.fwbchat.Entity.UserInfo;
import com.example.fwbchat.Server.APIService;
import com.example.fwbchat.Server.Data;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfile extends AppCompatActivity {
    private TextInputEditText etHoTen,etQuanHe,etDiaChi,etSDT,etNoiLamViec,etGioiThieu;
    private MaterialButton btnSaveProfile;
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private UserInfo user = new UserInfo();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        context = UpdateProfile.this;
        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        editor = sharedPref.edit();
        String uid = sharedPref.getString("uid","");

        etHoTen = findViewById(R.id.etHoTen);
        etQuanHe = findViewById(R.id.etQuanHe);
        etDiaChi = findViewById(R.id.etDiaChi);
        etSDT = findViewById(R.id.etSDT);
        etNoiLamViec = findViewById(R.id.etNoiLamViec);
        etGioiThieu = findViewById(R.id.etGioiThieu);
        btnSaveProfile = findViewById(R.id.btnSaveProfile);


        Data data = APIService.getService();
        Call<List<UserInfo>> callback = data.getUserInfo(uid);
        callback.enqueue(new Callback<List<UserInfo>>() {
            @Override
            public void onResponse(Call<List<UserInfo>> call, Response<List<UserInfo>> response) {
                List<UserInfo> userInfo = response.body();
                if(userInfo.size() > 0){
                    user= userInfo.get(0);
                    etHoTen.setText(user.getHoTen());
                    etQuanHe.setText(user.getMoiQuanHe() != null ? user.getMoiQuanHe().toString() : "");
                    etDiaChi.setText(user.getNoiSong()!= null ? user.getNoiSong().toString() : "");
                    etSDT.setText(user.getSoDT() != null ? user.getSoDT().toString() : "");
                    etNoiLamViec.setText(user.getNoiLamViec() != null ? user.getNoiLamViec().toString() : "");
                    etGioiThieu.setText(user.getGioiThieu() != null ? user.getGioiThieu().toString() : "");
                }
            }
            @Override
            public void onFailure(Call<List<UserInfo>> call, Throwable t) {
            }
        });


        btnSaveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String hoten = etHoTen.getText().toString();
                String quanhe = etQuanHe.getText().toString();
                String diachi = etDiaChi.getText().toString();
                String sdt = etSDT.getText().toString();
                String noilamviec = etNoiLamViec.getText().toString();
                String gioithieu = etGioiThieu.getText().toString();
                if(hoten.isEmpty()){
                    Toast.makeText(UpdateProfile.this,"Họ tên không được để trống!" , Toast.LENGTH_LONG).show();
                }else{
                    Call<String> callback = data.updateProfile(uid,hoten,quanhe,diachi,sdt,noilamviec,gioithieu);
                    callback.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            String rs = response.body();
                            if(rs.equals("Success")){
                                Toast.makeText(UpdateProfile.this,"Cập nhật hồ sơ thành công!" , Toast.LENGTH_LONG).show();
                                startActivity(new Intent(UpdateProfile.this,HomeActivity.class));
                            }else{
                                Toast.makeText(UpdateProfile.this,"Cập nhật hồ sơ thất bại!" , Toast.LENGTH_LONG).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                        }
                    });

                }
            }
        });
    }
    public void backHome(View view) {
        startActivity(new Intent(UpdateProfile.this,HomeActivity.class));
    }
}