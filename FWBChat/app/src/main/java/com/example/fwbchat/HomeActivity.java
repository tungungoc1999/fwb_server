package com.example.fwbchat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity {
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        context = HomeActivity.this;
        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        editor = sharedPref.edit();
        int idMenu = 0;
        try {
            idMenu = sharedPref.getInt("idMenu",0);
        }catch (Exception exx){

        }
        if(idMenu == 0){
            idMenu = R.id.menu_conversation;
        }
        Fragment(idMenu);
        String uid = sharedPref.getString("uid","");
        if(uid == ""){
            startActivity(new Intent(HomeActivity.this,LoginActivity.class));
        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        navigation.setSelectedItemId(idMenu);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            //khi click menu nhay ra frament tuong ung
            switch (item.getItemId()) {
                case R.id.menu_conversation:
                    Fragment(R.id.menu_conversation);
                    return true;
                case R.id.menu_call:
                    Fragment(R.id.menu_call);
                    return true;
                case R.id.menu_users:
                    Fragment(R.id.menu_users);
                    return true;
                case R.id.menu_group:
                    Fragment(R.id.menu_group);
                    return true;
                case R.id.menu_more:
                    Fragment(R.id.menu_more);
                    return true;
            }
            return false;
        }
    };
    private void Fragment(int i){
        switch (i){
            case R.id.menu_conversation:
                editor.putInt("idMenu",R.id.menu_conversation);
                editor.commit();
                FragmentConversation fragmentConversation = new FragmentConversation();
                if(findViewById(R.id.frame) != null){
                    getSupportFragmentManager().executePendingTransactions();
                    Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.frame);
                    if (fragmentById != null) {
                        getSupportFragmentManager().beginTransaction().remove(fragmentById).commit();
                    }
                    getSupportFragmentManager().beginTransaction().add(R.id.frame, fragmentConversation).commit();
                }
                Toast.makeText(HomeActivity.this,"menu_conversation" , Toast.LENGTH_LONG).show();
                return;
            case R.id.menu_call:
                editor.putInt("idMenu",R.id.menu_call);
                editor.commit();
                FragmentCall fragmentCall = new FragmentCall();
                if(findViewById(R.id.frame) != null){
                    getSupportFragmentManager().executePendingTransactions();
                    Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.frame);
                    if (fragmentById != null) {
                        getSupportFragmentManager().beginTransaction().remove(fragmentById).commit();
                    }
                    getSupportFragmentManager().beginTransaction().add(R.id.frame, fragmentCall).commit();
                }
                Toast.makeText(HomeActivity.this,"menu_call" , Toast.LENGTH_LONG).show();
                return;
            case R.id.menu_users:
                editor.putInt("idMenu",R.id.menu_users);
                editor.commit();
                FragmentUser fragmentUsers = new FragmentUser();
                if(findViewById(R.id.frame) != null){
                    getSupportFragmentManager().executePendingTransactions();
                    Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.frame);
                    if (fragmentById != null) {
                        getSupportFragmentManager().beginTransaction().remove(fragmentById).commit();
                    }
                    getSupportFragmentManager().beginTransaction().add(R.id.frame, fragmentUsers).commit();
                }
                Toast.makeText(HomeActivity.this,"menu_users", Toast.LENGTH_LONG).show();
                return;
            case R.id.menu_group:
                editor.putInt("idMenu",R.id.menu_group);
                editor.commit();
                FragmentGroup fragmentGroup = new FragmentGroup();
                if(findViewById(R.id.frame) != null){
                    getSupportFragmentManager().executePendingTransactions();
                    Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.frame);
                    if (fragmentById != null) {
                        getSupportFragmentManager().beginTransaction().remove(fragmentById).commit();
                    }
                    getSupportFragmentManager().beginTransaction().add(R.id.frame, fragmentGroup).commit();
                }
                Toast.makeText(HomeActivity.this,"menu_group" , Toast.LENGTH_LONG).show();
                return;
            case R.id.menu_more:
                editor.putInt("idMenu",R.id.menu_more);
                editor.commit();
                FragmentSetting fragmentSetting = new FragmentSetting();
                if(findViewById(R.id.frame) != null){
                    getSupportFragmentManager().executePendingTransactions();
                    Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.frame);
                    if (fragmentById != null) {
                        getSupportFragmentManager().beginTransaction().remove(fragmentById).commit();
                    }
                    getSupportFragmentManager().beginTransaction().add(R.id.frame, fragmentSetting).commit();
                }
                Toast.makeText(HomeActivity.this,"menu_more" , Toast.LENGTH_LONG).show();
                return;
        }
    }
    public void logOut(View view){
        editor.clear();
        editor.commit();
        Toast.makeText(HomeActivity.this,"Đăng xuất thành công" , Toast.LENGTH_LONG).show();
        startActivity(new Intent(HomeActivity.this,LoginActivity.class));
    }
    public void create_group(View view) {
        startActivity(new Intent(HomeActivity.this,Create_Group.class));
    }
    public void Profile(View view) {
        startActivity(new Intent(HomeActivity.this,UpdateProfile.class));
    }
}