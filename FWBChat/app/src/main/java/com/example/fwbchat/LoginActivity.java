package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.fwbchat.Entity.UserInfo;
import com.example.fwbchat.Server.APIService;
import com.example.fwbchat.Server.Data;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private TextInputLayout inputTaiKhoan,inputMatKhau;
    private TextInputEditText uidTaiKhoan,uidMatKhau;
    private MaterialButton loginBtn;
    private ProgressBar progressBar;
    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        uidTaiKhoan = findViewById(R.id.etTaiKhoan);
        uidMatKhau = findViewById(R.id.etMatKhau);
        loginBtn = findViewById(R.id.login);

        progressBar = findViewById(R.id.loginProgress);
        context  = LoginActivity.this;
        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        editor = sharedPref.edit();

        //lan dau vao check xem da dang nhap hay chua
        String uid = sharedPref.getString("uid","");
        if(uid != ""){
            Toast.makeText(LoginActivity.this,"Đăng nhập thành công" , Toast.LENGTH_LONG).show();
            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
        }

        //su kien login
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String taikhoan = uidTaiKhoan.getText().toString().trim();
                String matkhau = uidMatKhau.getText().toString().trim();
                if (taikhoan.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Tài khoản không được để trống!", Toast.LENGTH_LONG).show();
                }else if (matkhau.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Mật khẩu không được để trống!", Toast.LENGTH_LONG).show();
                }else{
                    Data data = APIService.getService();
                    Call<List<UserInfo>> callback = data.checkLogin(taikhoan,matkhau);
                    callback.enqueue(new Callback<List<UserInfo>>() {
                        @Override
                        public void onResponse(Call<List<UserInfo>> call, Response<List<UserInfo>> response) {
                            List<UserInfo> user = response.body();
                            Toast.makeText(LoginActivity.this, "123!", Toast.LENGTH_LONG).show();
                            if(user.size() != 0){
                                Toast.makeText(LoginActivity.this,"Đăng nhập thành công" , Toast.LENGTH_LONG).show();
                                editor.putString("uid",user.get(0).getId());
                                editor.putString("ten",user.get(0).getHoTen());
                                editor.commit();
                                startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                            }else{
                                Toast.makeText(LoginActivity.this,"Sai tài khoản hoặc mật khẩu" , Toast.LENGTH_LONG).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<List<UserInfo>> call, Throwable t) {
                            Toast.makeText(LoginActivity.this,"Lỗi đăng nhập", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    //su kien vao trang dang ky
    public void createUser(View view) {
        startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
    }
}
