package com.example.fwbchat.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserInfo {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("TaiKhoan")
    @Expose
    private String taiKhoan;
    @SerializedName("MatKhau")
    @Expose
    private String matKhau;
    @SerializedName("HoTen")
    @Expose
    private String hoTen;
    @SerializedName("TrangThai")
    @Expose
    private Object trangThai;
    @SerializedName("lastActive")
    @Expose
    private String lastActive;
    @SerializedName("MoiQuanHe")
    @Expose
    private Object moiQuanHe;
    @SerializedName("SoDT")
    @Expose
    private Object soDT;
    @SerializedName("NoiSong")
    @Expose
    private Object noiSong;
    @SerializedName("NoiLamViec")
    @Expose
    private Object noiLamViec;
    @SerializedName("GioiThieu")
    @Expose
    private Object gioiThieu;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaiKhoan() {
        return taiKhoan;
    }

    public void setTaiKhoan(String taiKhoan) {
        this.taiKhoan = taiKhoan;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public Object getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Object trangThai) {
        this.trangThai = trangThai;
    }

    public String getLastActive() {
        return lastActive;
    }

    public void setLastActive(String lastActive) {
        this.lastActive = lastActive;
    }

    public Object getMoiQuanHe() {
        return moiQuanHe;
    }

    public void setMoiQuanHe(Object moiQuanHe) {
        this.moiQuanHe = moiQuanHe;
    }

    public Object getSoDT() {
        return soDT;
    }

    public void setSoDT(Object soDT) {
        this.soDT = soDT;
    }

    public Object getNoiSong() {
        return noiSong;
    }

    public void setNoiSong(Object noiSong) {
        this.noiSong = noiSong;
    }

    public Object getNoiLamViec() {
        return noiLamViec;
    }

    public void setNoiLamViec(Object noiLamViec) {
        this.noiLamViec = noiLamViec;
    }

    public Object getGioiThieu() {
        return gioiThieu;
    }

    public void setGioiThieu(Object gioiThieu) {
        this.gioiThieu = gioiThieu;
    }
    @Override
    public String toString() {
        return hoTen;
    }
}
