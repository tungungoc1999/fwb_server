package com.example.fwbchat.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroupChat {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("TenNhom")
    @Expose
    private String tenNhom;
    @SerializedName("NgayTao")
    @Expose
    private String ngayTao;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("IdNguoiTao")
    @Expose
    private String idNguoiTao;
    @SerializedName("TenNguoiTao")
    @Expose
    private String tenNguoiTao;
    @SerializedName("MoTa")
    @Expose
    private String moTa;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenNhom() {
        return tenNhom;
    }

    public void setTenNhom(String tenNhom) {
        this.tenNhom = tenNhom;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdNguoiTao() {
        return idNguoiTao;
    }

    public void setIdNguoiTao(String idNguoiTao) {
        this.idNguoiTao = idNguoiTao;
    }

    public String getTenNguoiTao() {
        return tenNguoiTao;
    }

    public void setTenNguoiTao(String tenNguoiTao) {
        this.tenNguoiTao = tenNguoiTao;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    @Override
    public String toString() {
        return tenNhom;
    }

}
