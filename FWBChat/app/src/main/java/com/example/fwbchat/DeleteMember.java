package com.example.fwbchat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import com.example.fwbchat.Entity.UserInfo;
import com.example.fwbchat.Server.APIService;
import com.example.fwbchat.Server.Data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteMember extends AppCompatActivity {
    private Context context = DeleteMember.this;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private ArrayList<UserInfo> lstUser = new ArrayList<UserInfo>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_member);
        sharedPref = context.getSharedPreferences("userLogin",MODE_PRIVATE);
        String idGroup = sharedPref.getString("idGroup","");
        String uid = sharedPref.getString("uid","");
        ListView myListView = findViewById(R.id.listMemberDelete);
        Data data = APIService.getService();
        Call<List<UserInfo>> callback = data.getListMemberInGroup(idGroup);
        callback.enqueue(new Callback<List<UserInfo>>() {
            @Override
            public void onResponse(Call<List<UserInfo>> call, Response<List<UserInfo>> response) {
                lstUser = (ArrayList<UserInfo>) response.body();
                ArrayAdapter adapter = new ArrayAdapter(DeleteMember.this, android.R.layout.simple_list_item_1,lstUser);
                myListView.setAdapter(adapter);
            }
            @Override
            public void onFailure(Call<List<UserInfo>> call, Throwable t) {
                Toast.makeText(DeleteMember.this,"Lỗi", Toast.LENGTH_LONG).show();
            }
        });


        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserInfo user = lstUser.get(position);
                Call<String> deleteM = data.deleteMemberGroup(idGroup,user.getId());
                deleteM.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String res = response.body();
                        if(res.equals("Success")){
                            Toast.makeText(DeleteMember.this,"Xóa thành viên thành công" , Toast.LENGTH_LONG).show();
                            startActivity(new Intent(DeleteMember.this,DeleteMember.class));
                        }
                    }
                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });
            }
        });
    }
    public void backToDetailH(View view) {
        startActivity(new Intent(DeleteMember.this,DetailGroup.class));
    }
}