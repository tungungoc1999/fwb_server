package com.example.fwbchat.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageChat {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("IdNguoiGui")
    @Expose
    private String idNguoiGui;
    @SerializedName("TenNguoiGui")
    @Expose
    private String tenNguoiGui;
    @SerializedName("KieuTinNhan")
    @Expose
    private String kieuTinNhan;
    @SerializedName("IdNguoiNhan")
    @Expose
    private String idNguoiNhan;
    @SerializedName("TenNguoiNhan")
    @Expose
    private String tenNguoiNhan;
    @SerializedName("ThoiGianGui")
    @Expose
    private String thoiGianGui;
    @SerializedName("TrangThai")
    @Expose
    private String trangThai;
    @SerializedName("CountChuaDoc")
    @Expose
    private String countChuaDoc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdNguoiGui() {
        return idNguoiGui;
    }

    public void setIdNguoiGui(String idNguoiGui) {
        this.idNguoiGui = idNguoiGui;
    }

    public String getTenNguoiGui() {
        return tenNguoiGui;
    }

    public void setTenNguoiGui(String tenNguoiGui) {
        this.tenNguoiGui = tenNguoiGui;
    }

    public String getKieuTinNhan() {
        return kieuTinNhan;
    }

    public void setKieuTinNhan(String kieuTinNhan) {
        this.kieuTinNhan = kieuTinNhan;
    }

    public String getIdNguoiNhan() {
        return idNguoiNhan;
    }

    public void setIdNguoiNhan(String idNguoiNhan) {
        this.idNguoiNhan = idNguoiNhan;
    }

    public String getTenNguoiNhan() {
        return tenNguoiNhan;
    }

    public void setTenNguoiNhan(String tenNguoiNhan) {
        this.tenNguoiNhan = tenNguoiNhan;
    }

    public String getThoiGianGui() {
        return thoiGianGui;
    }

    public void setThoiGianGui(String thoiGianGui) {
        this.thoiGianGui = thoiGianGui;
    }

    public String getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(String trangThai) {
        this.trangThai = trangThai;
    }

    public String getCountChuaDoc() {
        return countChuaDoc;
    }

    public void setCountChuaDoc(String countChuaDoc) {
        this.countChuaDoc = countChuaDoc;
    }

}
