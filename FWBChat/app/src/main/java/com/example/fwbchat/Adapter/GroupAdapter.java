package com.example.fwbchat.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.fwbchat.Entity.GroupChat;
import com.example.fwbchat.R;

import java.util.ArrayList;

public class GroupAdapter extends ArrayAdapter<GroupChat> {
    private Context context;
    private int resource;
    private ArrayList<GroupChat> ar;
    public GroupAdapter(Context context, int simple_list_item_1, ArrayList<GroupChat> countryList) {
        super(context, 0, countryList);
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.item_user, parent, false
        );
//        Group currentItem = getItem(position);
//        if(currentItem !=null){
//            TextView txtUserName = convertView.findViewById(R.id.txt_user_name);
//            txtUserName.setText(currentItem.getTenNhom());
//        }
        return convertView;
    }
}
